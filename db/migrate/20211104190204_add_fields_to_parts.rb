class AddFieldsToParts < ActiveRecord::Migration[6.1]
  def change
    add_column :parts, :price, :decimal
    add_column :parts, :notes, :string
    add_column :parts, :link1, :string
    add_column :parts, :link2, :string
    add_column :parts, :link3, :string
  end
end
