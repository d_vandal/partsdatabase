class AddItemNumberToParts < ActiveRecord::Migration[6.1]
  def change
    add_column :parts, :item_number, :string
  end
end
