class AddViewToPart < ActiveRecord::Migration[6.1]
  def change
    add_column :parts, :views, :integer
  end
end
