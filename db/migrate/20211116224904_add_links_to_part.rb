class AddLinksToPart < ActiveRecord::Migration[6.1]
  def change
    add_column :parts, :link1_title, :string
    add_column :parts, :link2_title, :string
    add_column :parts, :link3_title, :string
  end
end
