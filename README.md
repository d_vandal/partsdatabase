# PartsDatabase

This is a web application for creating a general purpose data store of components and support documentation for a product support department. This is a resource for agents to allow quick access to information about components, as well as some troubleshooting applications.

This is a work in progress. At present you can create parts with an attachment and search them via tags.

# How to run:
Clone the repository.

Run:

``` 
# Initialize the Rails Application
cd partsdatabase/
bin/bundle install
bin/rails db:migrate

# Install Tailwind CSS
yarn add tailwindcss@npm:@tailwindcss/postcss7-compat postcss@7 autoprefixer@9
npx tailwindcss init --full
bin/rails assets:precompile
 ```

Start the server with ```bin/rails s``` and start uploading parts.

WIP.
