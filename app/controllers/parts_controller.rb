class PartsController < ApplicationController
  before_action :set_part, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, except: [:picture]
  protect_from_forgery unless: -> { request.format.json? }

  # GET /parts or /parts.json
  def index
    @parts = []
    # Handles GET requests off of tags/<tag> to just utilize the parts index,
    # rather than rendering a different view just for that.
    if params[:tag]
      @parts = Part.tagged_with(params[:tag])
    # Fetches the 10 most linked/viewed pictures
    else
      parts = Part.all
      parts = parts.order('views')
      @parts = parts.reverse
    end

    @parts = Kaminari.paginate_array(@parts).page(params[:page]).per(10)
  end

  # GET /parts/1 or /parts/1.json
  def show
    if @part.views == nil
      @part.views = 0
    end
    @part.views += 1
    @part.save
  end

  # GET /parts/new
  def new
    @part = Part.new
  end

  # GET /parts/1/edit
  def edit
  end

  # POST /parts or /parts.json
  def create
    @part = Part.new(part_params)

    respond_to do |format|
      if @part.save
        format.html { redirect_to @part, notice: "Part was successfully created." }
        format.json { render :show, status: :created, location: @part }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /parts/1 or /parts/1.json
  def update
    respond_to do |format|
      if @part.update(part_params)
        format.html { redirect_to @part, notice: "Part was successfully updated." }
        format.json { render :show, status: :ok, location: @part }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @part.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /parts/1 or /parts/1.json
  # Every time a part is deleted, it'll prune the database of
  # any tags that have no parts associated
  def destroy
    @part.destroy
    Tag.all.each do |tag|
      if tag.parts.length == 0
        tag.destroy
      end
    end
    respond_to do |format|
      format.html { redirect_to parts_url, notice: "Part was successfully destroyed." }
      format.json { head :no_content }
    end
  end


  # GET /search/query=<query>&commit=Search
  # Utilizes the tagged_with method off of Parts to get every Part with the supplied tag
  # it then trims the list by making sure all searched tags are present on the item
  # Then prunes the list of any duplicates
  # It returns the search.js.erb after for client side rendering
  def search
    @query = params[:query].downcase
    @parts = []
    if @query != nil
      tags = @query.split(",").map
      tags.each do |tag|
        @parts += Part.tagged_with(tag.strip)
      end
    else
      @parts = Part.all
    end
    
    # exclusive searching
    # tags.each do |tag|
    #   @parts.delete_if { |part| !part.all_tags.include? tag.strip }
    # end


    @parts = @parts.uniq
    # sorts the results by the number of matching tags,
    # see num_tags at the bottom for more info
    @parts = @parts.sort_by {|part| num_tags(part, tags)}
    

    
    @parts = Kaminari.paginate_array(@parts).page(params[:page]).per(10)
    respond_to do |format|
      format.js
    end
  end

  def all_tags
    @tags = Tag.all

    respond_to do |format|
      format.js
    end
  end

  def picture
    @part = Part.find(params[:id])
    if @part != nil
      redirect_to url_for(@part.picture)
    else
      redirect_to 'index'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_part
      @part = Part.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def part_params
      params.require(:part).permit(:name, :picture, :item_number, :all_tags, :price, :notes, :link1, :link2, :link3, :link1_title, :link2_title, :link3_title)
    end

    # sort_by orders smallest to largest
    # so we count negatively, the one with
    # the lowest number is ordered to the top
    # and the highest is ordered to the bottom
    # calling reverse after is another step, so
    # just doing it here instead skips that
    def num_tags(part, tags)
      count = 0
      tags.each do |tag|
        if part.all_tags.include? tag
          count -= 1
        end
      end
      return count
    end
end
