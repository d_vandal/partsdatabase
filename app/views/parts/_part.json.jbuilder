json.extract! part, :id, :name, :picture, :created_at, :updated_at
json.url part_url(part, format: :json)
json.picture url_for(part.picture)
