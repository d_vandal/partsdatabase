class Tagging < ApplicationRecord
  belongs_to :part
  belongs_to :tag
end
