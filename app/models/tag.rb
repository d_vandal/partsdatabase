class Tag < ApplicationRecord
  has_many :taggings
  has_many :parts, through: :taggings
end
