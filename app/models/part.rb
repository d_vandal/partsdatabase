class Part < ApplicationRecord
  has_one_attached :picture
  has_many :taggings, dependent: :delete_all
  has_many :tags, through: :taggings
  validates :name, :picture, :item_number, presence: true
  paginates_per 10

  # This handles both creating the new tags and
  # mapping them to the Part through Tagging
  def all_tags=(name)
    self.tags = name.strip.split(",").map do |name|
      if name != " "
        Tag.where(name: name.strip.downcase).first_or_create!
      end
    end
  end

  # Gets all of the tags under a part and converts all
  # of the names to a comma delimited string.
  # This is mainly for use in views
  def all_tags
    self.tags.map(&:name).join(", ")
  end

  # 
  def self.tagged_with(name)
    if Tag.find_by_name(name) != nil
      Tag.find_by_name(name).parts
    else
      return []
    end
  end
end
